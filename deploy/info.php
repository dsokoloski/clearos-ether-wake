<?php

/////////////////////////////////////////////////////////////////////////////
// General information
/////////////////////////////////////////////////////////////////////////////

$app['basename'] = 'ether_wake';
$app['version'] = '1.0.0';
$app['release'] = '1';
$app['vendor'] = 'ClearFoundation';
$app['packager'] = 'Darryl Sokoloski <dsokoloski@clearfoundation.com>';
$app['license'] = 'GPLv3';
$app['license_core'] = 'LGPLv3';
$app['description'] = lang('ether_wake_app_description');

/////////////////////////////////////////////////////////////////////////////
// App name and categories
/////////////////////////////////////////////////////////////////////////////

$app['name'] = lang('ether_wake_app_name');
$app['category'] = lang('base_category_network');
$app['subcategory'] = lang('ether_wake_subcategory_management_tools');

/////////////////////////////////////////////////////////////////////////////
// Packaging
/////////////////////////////////////////////////////////////////////////////

$app['core_requires'] = array(
    'net-tools', 
    'app-tasks-core', 
    'app-network-core >= 1:1.4.38', 
);

$app['core_file_manifest'] = array(
    'ether_wake.conf' => array(
        'target' => '/etc/clearos/ether_wake.conf',
        'config' => TRUE,
        'config_params' => 'noreplace',
    ),
);

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
