#!/usr/clearos/sandbox/usr/bin/php
<?php

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = isset($_ENV['CLEAROS_BOOTSTRAP']) ?
    $_ENV['CLEAROS_BOOTSTRAP'] : '/usr/clearos/framework/shared';
require_once($bootstrap . '/bootstrap.php');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

// Classes
//--------

use \clearos\apps\ether_wake\Ether_Wake as Ether_Wake;

clearos_load_library('ether_wake/Ether_Wake');

// Exceptions
//-----------

use \clearos\apps\ether_wake\Device_Already_Exists_Exception as Device_Already_Exists_Exception;
use \clearos\apps\ether_wake\Device_Not_Found_Exception as Device_Not_Found_Exception;
use \clearos\apps\ether_wake\Invalid_Identifier_Exception as Invalid_Identifier_Exception;
use \clearos\apps\ether_wake\Invalid_Password_Exception as Invalid_Password_Exception;
use \clearos\apps\ether_wake\Send_Wake_Exception as Send_Wake_Exception;
use \Exception as Exception;

clearos_load_library('ether_wake/Device_Already_Exists_Exception');
clearos_load_library('ether_wake/Device_Not_Found_Exception');
clearos_load_library('ether_wake/Invalid_Identifier_Exception');
clearos_load_library('ether_wake/Invalid_Password_Exception');
clearos_load_library('ether_wake/Send_Wake_Exception');

///////////////////////////////////////////////////////////////////////////////
// E T H E R - W A K E
///////////////////////////////////////////////////////////////////////////////

const MODE_ADD = 1;
const MODE_DELETE = 2;
const MODE_WAKE = 3;
const MODE_LIST = 4;

$shortopts = 'h'; // Display usage help
$shortopts .= 'a'; // Add device
$shortopts .= 'd'; // Delete device
$shortopts .= 'w'; // Wake device
$shortopts .= 'l'; // List devices
$shortopts .= 'm:'; // Device's MAC address or identifier (from /etc/ethers).
$shortopts .= 'i:'; // Interface name
$shortopts .= 'b'; // Use broadcast address?
$shortopts .= 'p:'; // Password to use (optional).

$longopts=array(
    'help',
    'add',
    'delete',
    'wake',
    'list',
    'mac:',
    'broadcast',
    'password:',
);

if (($options = getopt($shortopts, $longopts)) === FALSE) {
    printf("Error parsing arguments.\n");
    exit(1);
}

$mode = NULL;

if (array_key_exists('h', $options) ||
    array_key_exists('help', $options)) {
    printf("Usage for %s <mode> [<options>]\n", basename($argv[0]));
    printf("\nModes\n");
    printf("  -a, --add        Configure a new device for Wake-on-LAN.\n");
    printf("  -d, --delete     Delete a device from configuration file.\n");
    printf("  -w, --wake       Send a Wake-on-LAN \"Magic Packet\" to a device.\n");
    printf("  -l, --list       List all configured devices.\n");
    printf("\nOptions\n");
    printf("  -m, --mac        MAC address or identifier from /etc/ethers.\n");
    printf("  -i, --interface  Interface to use when sending a \"Magic Packet\".\n");
    printf("  -b, --broadcast  Use interface's broadcast address?\n");
    printf("  -p, --password   Optional plain-text password.\n");
    exit(0);
}

if (array_key_exists('a', $options) ||
    array_key_exists('add', $options)) $mode = MODE_ADD;
if (array_key_exists('d', $options) ||
    array_key_exists('delete', $options)) {
    if ($mode != NULL) {
        printf("Duplicate mode detected.\n");
        exit(1);
    }
    $mode = MODE_DELETE;
}
if (array_key_exists('w', $options) ||
    array_key_exists('wake', $options)) {
    if ($mode != NULL) {
        printf("Duplicate mode detected.\n");
        exit(1);
    }
    $mode = MODE_WAKE;
}
if (array_key_exists('l', $options) ||
    array_key_exists('list', $options)) {
    if ($mode != NULL) {
        printf("Duplicate mode detected.\n");
        exit(1);
    }
    $mode = MODE_LIST;
}
if ($mode == NULL) {
    printf("No mode specified.\n");
    exit(1);
}

$ether_wake = new Ether_Wake();

if ($mode == MODE_ADD) {
    $mac = NULL;
    $interface = NULL;
    $broadcast = FALSE;
    $password = NULL;

    if (array_key_exists('m', $options)) $mac = $options['m'];
    else if (array_key_exists('mac', $options)) $mac = $options['mac'];
    if (array_key_exists('i', $options)) $interface = $options['i'];
    else if (array_key_exists('interface', $options)) $interface = $options['interface'];
    if (array_key_exists('b', $options)) $broadcast = TRUE;
    else if (array_key_exists('broadcast', $options)) $broadcast = TRUE;
    if (array_key_exists('p', $options)) $password = $options['p'];
    else if (array_key_exists('password', $options)) $password = $options['password'];

    if ($mac == NULL || !strlen($mac)) {
        printf("Required parameter missing: MAC address or identifier.\n");
        exit(1);
    }
    if ($interface == NULL || !strlen($interface)) {
        printf("Required parameter missing: interface.\n");
        exit(1);
    }
    if ($password == NULL) $password = '';

    try {
        $ether_wake->add_device($mac, $interface, $broadcast, $password);
    } catch (Invalid_Identifier_Exception $e) {
        printf("%s: %s\n", $e->getMessage(), $e->getIdentifier());
        exit(1);
    } catch (Invalid_Password_Exception $e) {
        printf("%s: %s\n", $e->getMessage(), $e->getPassword());
        exit(1);
    } catch (Device_Already_Exists_Exception $e) {
        printf("%s: %s\n", $e->getMessage(), $e->getIdentifier());
        exit(1);
    } catch (Engine_Exception $e) {
        printf("Unexpected exception: %s\n", $e->getMessage());
        exit(1);
    }

    printf("Device added successfully.\n");
}
else if ($mode == MODE_DELETE) {
    $mac = NULL;

    if (array_key_exists('m', $options)) $mac = $options['m'];
    else if (array_key_exists('mac', $options)) $mac = $options['mac'];

    if ($mac == NULL || !strlen($mac)) {
        printf("Required parameter missing: MAC address or identifier.\n");
        exit(1);
    }

    try {
        $ether_wake->delete_device($mac);
    } catch (Invalid_Identifier_Exception $e) {
        printf("%s: %s\n", $e->getMessage(), $e->getIdentifier());
        exit(1);
    } catch (Device_Not_Found_Exception $e) {
        printf("%s: %s\n", $e->getMessage(), $e->getIdentifier());
        exit(1);
    } catch (Engine_Exception $e) {
        printf("Unexpected exception: %s\n", $e->getMessage());
        exit(1);
    }

    printf("Device deleted.\n");
}
else if ($mode == MODE_WAKE) {
    $mac = NULL;

    if (array_key_exists('m', $options)) $mac = $options['m'];
    else if (array_key_exists('mac', $options)) $mac = $options['mac'];

    if ($mac == NULL || !strlen($mac)) {
        printf("Required parameter missing: MAC address or identifier.\n");
        exit(1);
    }

    try {
        $ether_wake->wake_device($mac);
    } catch (Invalid_Identifier_Exception $e) {
        printf("%s: %s\n", $e->getMessage(), $e->getIdentifier());
        exit(1);
    } catch (Device_Not_Found_Exception $e) {
        printf("%s: %s\n", $e->getMessage(), $e->getIdentifier());
        exit(1);
    } catch (Send_Wake_Exception $e) {
        printf("%s (%d).\n", $e->getMessage(), $e->getReturnCode());
        exit(1);
    } catch (Engine_Exception $e) {
        printf("Unexpected exception: %s\n", $e->getMessage());
        exit(1);
    }

    printf("Sent \"Magic Packet\" to device.\n");
}
else if ($mode == MODE_LIST) {
    try {
        $devices = $ether_wake->get_device_list();
        if (!count($devices))
            printf("No configured devices found.\n");
        else {
            printf("%-17s %-9s %-9s %-8s\n",
                'Device', 'Interface', 'Broadcast', 'Password');
            foreach ($devices as $mac => $config) {
                printf("%-17s %-9s %-9s %-8s\n",
                    $mac, $config['interface'],
                    ($config['broadcast']) ? 'Yes' : 'No',
                    (strlen($config['password']) ?
                        $config['password'] : 'None'));
            }
        }
    } catch (Engine_Exception $e) {
        printf("Unexpected exception: %s\n", $e->getMessage());
        exit(1);
    }
}


exit(0);

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
