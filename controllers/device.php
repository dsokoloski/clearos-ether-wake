<?php

/**
 * Device controller.
 *
 * @category   Apps
 * @package    Ether_Wake
 * @subpackage Controllers
 * @author     Darryl Sokoloski <dsokoloski@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    GPLv3
 */

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Device controller.
 *
 * @category   Apps
 * @package    Ether_Wake
 * @subpackage Controllers
 * @author     Darryl Sokoloski <dsokoloski@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    GPLv3
 */

class Device extends ClearOS_Controller
{
    /**
     * Device controller.
     *
     * @return view
     */

    function index()
    {
        // Load dependencies
        //------------------

        $this->load->library('ether_wake/Ether_Wake');

        // Load views
        //-----------

        $data = array('devices' => $this->ether_wake->get_device_list());
        $this->page->view_form(
            'ether_wake/device', $data, lang('ether_wake_app_name')
        );
    }

    /**
     * Add device controller.
     *
     * @return view
     */

    function add()
    {
        $this->_add_edit('add');
    }

    /**
     * Edit device controller.
     *
     * @return view
     */

    function edit($ident)
    {
        $this->_add_edit('edit', urldecode($ident));
    }

    /**
     * Delete device controller.
     *
     * @return view
     */

    function delete($ident)
    {
        // Load dependencies
        //------------------

        $this->load->library('ether_wake/Ether_Wake');

        // Delete device
        //--------------

        try {
            $this->ether_wake->delete_device(urldecode($ident));
            redirect('/ether_wake/ether_wake');
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }
    }

    /**
     * Wake device controller.
     *
     * @return view
     */

    function wake($ident)
    {
        // Load dependencies
        //------------------

        $this->load->library('ether_wake/Ether_Wake');

        // Wake device
        //------------

        try {
            $this->ether_wake->wake_device(urldecode($ident));
            redirect('/ether_wake/ether_wake');
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }
    }

    /**
     * Wake all device controller.
     *
     * @return view
     */

    function wake_all()
    {
        // Load dependencies
        //------------------

        $this->load->library('ether_wake/Ether_Wake');

        // Wake all devices
        //-----------------

        try {
            $devices = $this->ether_wake->get_device_list();
            foreach ($devices as $ident => $config)
                $this->ether_wake->wake_device($ident);
            redirect('/ether_wake/ether_wake');
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }
    }

    /**
     * Add/edit device controller.
     *
     * @access private
     * @return view
     */

    function _add_edit($mode, $ident = NULL)
    {
        // Load dependencies
        //------------------

        $this->load->library('ether_wake/Ether_Wake');
        $this->load->library('network/Iface_Manager');
        $this->load->library('network/Ethers');

        // Set validation rules
        //---------------------

        $this->form_validation->set_policy(
            'identifier', 'ether_wake/Ether_Wake',
            'validate_ident', TRUE
        );
        $this->form_validation->set_policy(
            'password', 'ether_wake/Ether_Wake',
            'validate_password', FALSE
        );

        $form_ok = $this->form_validation->run();

        // Handle form submit
        //-------------------

        if ($this->input->post('submit-form') && ($form_ok === TRUE)) {
            try {
                if ($mode == 'edit') {
                    $this->ether_wake->delete_device(
                        $this->input->post('identifier'));
                }

                $this->ether_wake->add_device(
                    $this->input->post('identifier'),
                    $this->input->post('interface'),
                    ($this->input->post('broadcast') == 'on') ?
                        TRUE : FALSE,
                    $this->input->post('password')
                );

                if ($this->input->post('schedule') == 'on') {
                    $this->ether_wake->set_schedule(
                        $this->input->post('identifier'),
                        '0', // minute
                        $this->input->post('hour'),
                        '*', // day_of_month
                        '*', // month
                        '*'  // day_of_week
                    );
                }

                redirect('/ether_wake/ether_wake');
            } catch (Exception $e) {
                $this->page->view_exception($e);
                return;
            }
        }

        // Load data
        //----------

        $data = array('mode' => $mode);
        $data['ethers'] = $this->ethers->get_ethers();
        $data['ethers'][] = '';
        $data['interfaces'] = $this->iface_manager->get_interfaces(
            array('filter_tun' => FALSE, 'filter_ppp' => TRUE)
        );
        if ($mode == 'add')
            $data['title'] = lang('ether_wake_device_add');
        else {
            $data['title'] = lang('ether_wake_device_edit');

            $data['ident'] = $ident;
            $data['device'] = $this->ether_wake->get_device($ident);
            $data['schedule'] = $this->ether_wake->get_schedule($ident);
        }

        // Load views
        //-----------

        $this->page->view_form(
            'ether_wake/device_edit', $data, $data['title']
        );
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
