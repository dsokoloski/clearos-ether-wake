<?php

/**
 * Ether Wake class.
 *
 * @category   Apps
 * @package    Ether Wake
 * @subpackage Libraries
 * @author     Darryl Sokoloski <dsokoloski@clearfoundation.com>
 * @copyright  2013 Darryl Sokoloski
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/ether_wake/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\ether_wake;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ?
    getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';

require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('ether_wake');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

// Classes
//--------

use \clearos\apps\base\Engine as Engine;
use \clearos\apps\base\File as File;
use \clearos\apps\base\Folder as Folder;
use \clearos\apps\base\Shell as Shell;
use \clearos\apps\base\Webconfig as Webconfig;
use \clearos\apps\network\Ethers as Ethers;
use \clearos\apps\network\Network_Utils as Network_Utils;
use \clearos\apps\tasks\Cron as Cron;

clearos_load_library('base/Engine');
clearos_load_library('base/File');
clearos_load_library('base/Folder');
clearos_load_library('base/Shell');
clearos_load_library('base/Webconfig');
clearos_load_library('network/Ethers');
clearos_load_library('network/Network_Utils');
clearos_load_library('tasks/Cron');

// Exceptions
//-----------

use \clearos\apps\base\Engine_Exception as Engine_Exception;
use \clearos\apps\base\File_No_Match_Exception as File_No_Match_Exception;
use \clearos\apps\base\File_Not_Found_Exception as File_Not_Found_Exception;
use \clearos\apps\base\Folder_Not_Found_Exception as Folder_Not_Found_Exception;
use \clearos\apps\base\Validation_Exception as Validation_Exception;
use \clearos\apps\ether_wake\Device_Already_Exists_Exception as Device_Already_Exists_Exception;
use \clearos\apps\ether_wake\Device_Not_Found_Exception as Device_Not_Found_Exception;
use \clearos\apps\ether_wake\Invalid_Identifier_Exception as Invalid_Identifier_Exception;
use \clearos\apps\ether_wake\Invalid_Password_Exception as Invalid_Password_Exception;
use \clearos\apps\ether_wake\Send_Wake_Exception as Send_Wake_Exception;
use \clearos\apps\network\Ethers_Not_Found_Exception as Ethers_Not_Found_Exception;
use \clearos\apps\tasks\Cron_Configlet_Not_Found_Exception as Cron_Configlet_Not_Found_Exception;
use \Exception as Exception;

clearos_load_library('base/Engine_Exception');
clearos_load_library('base/File_No_Match_Exception');
clearos_load_library('base/File_Not_Found_Exception');
clearos_load_library('base/Folder_Not_Found_Exception');
clearos_load_library('base/Validation_Exception');
clearos_load_library('ether_wake/Device_Already_Exists_Exception');
clearos_load_library('ether_wake/Device_Not_Found_Exception');
clearos_load_library('ether_wake/Invalid_Identifier_Exception');
clearos_load_library('ether_wake/Invalid_Password_Exception');
clearos_load_library('ether_wake/Send_Wake_Exception');
clearos_load_library('network/Ethers_Not_Found_Exception');
clearos_load_library('tasks/Cron_Configlet_Not_Found_Exception');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Ether Wake class.
 *
 * @category   Apps
 * @package    Ether Wake
 * @subpackage Libraries
 * @author     Darryl Sokoloski <dsokoloski@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/ether_wake/
 */

class Ether_Wake extends Engine
{
    ///////////////////////////////////////////////////////////////////////////////
    // C O N S T A N T S
    ///////////////////////////////////////////////////////////////////////////////

    const FILE_CONFIG = '/etc/clearos/ether_wake.conf';
    const BIN_ETHER_WAKE = '/sbin/ether-wake';

    ///////////////////////////////////////////////////////////////////////////////
    // V A R I A B L E S
    ///////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////
    // M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Ether Wake constructor.
     *
     * @return void
     */

    public function __construct()
    {
        clearos_profile(__METHOD__, __LINE__);

        $file = new File(self::FILE_CONFIG);
        if (! $file->exists()) {
            $file->create('root', 'root', '0640');
            $lines = "# Ether Wake (app-ether-wake) configuration file.\n\n";
            $file->add_lines($lines);
        }
    }

    /**
     * Resolve an identifier in to a MAC address.
     *
     * @param string $ident Device identifier
     *
     * @return string MAC address.
     * @throws Invalid_Identifier_Exception
     */

    public static function resolve_ident($ident)
    {
        if (Network_Utils::is_valid_mac($ident))
            return $ident;

        $mac = NULL;

        try {
            $ethers = new Ethers();
            $mac = $ethers->get_mac_by_hostname($ident);
        } catch (Engine_Exception $e) {
        }

        if ($mac == NULL)
            throw new Invalid_Identifier_Exception($ident, CLEAROS_ERROR);
        return $mac;
    }

    /**
     * Add device (by MAC address or identifier) to wake.
     *
     * @param string $ident Device identifier.
     * @param string $ifn Network interface to write magic packet to.
     * @param boolean $broadcast Send packet via interface's broadcast address?
     * @param string $password Optional password.
     *
     * @return void
     * @throws Invalid_Identifier_Exception, Invalid_Password_Exception, Device_Already_Exists_Exception
     */

    public function add_device($ident, $ifn, $broadcast = FALSE, $password = '')
    {
        $mac = $this->resolve_ident($ident);
        if (strlen($password) && $this->is_valid_password($password) === FALSE)
            throw new Invalid_Password_Exception($password, CLEAROS_ERROR);

        $file = new File(self::FILE_CONFIG, TRUE);
        if (! $file->exists()) return FALSE;

        $devices = $this->get_device_list();

        if (array_key_exists($mac, $devices) === FALSE) {
            $devices[$mac]['interface'] = $ifn;
            $devices[$mac]['broadcast'] = ($broadcast === TRUE) ? 1 : 0;
            $devices[$mac]['password'] = $password;
            $lines = '';
            ksort($devices);
            foreach ($devices as $key => $config)
                $lines .= sprintf("ether_wake_device[\"%s\"]=\"%s|%d|%s\"\n",
                    $key, $config['interface'],
                    $config['broadcast'], $config['password']);
            $file->delete_lines('/^ether_wake_device/');
            $file->add_lines($lines);

            return;
        }

        throw new Device_Already_Exists_Exception($ident, CLEAROS_ERROR);
    }

    /**
     * Delete device (by MAC address) from list.
     *
     * @param string $ident Identifier or MAC address of device to delete
     *
     * @return void
     * @throws Invalid_Identifier_Exception, Device_Not_Found_Exception
     */

    public function delete_device($ident)
    {
        $mac = $this->resolve_ident($ident);
        $file = new File(self::FILE_CONFIG);
        if (! $file->exists()) return FALSE;
        $devices = $this->get_device_list();
        if (array_key_exists($mac, $devices) !== FALSE) {
            unset($devices[$mac]);
            $lines = '';
            ksort($devices);
            foreach ($devices as $key => $config)
                $lines .= sprintf(
                    "ether_wake_device[\"%s\"]=\"%s|%d|%s\"\n",
                    $key, $config['interface'],
                    $config['broadcast'], $config['password']);
            $file->delete_lines('/^ether_wake_device/');
            $file->add_lines($lines);

            $name = sprintf(
                'app-ether-wake-%s', preg_replace('/:/', '-', $mac));

            $cron = new Cron();
            $cron->delete_configlet($name);

            return;
        }

        throw new Device_Not_Found_Exception($ident, CLEAROS_ERROR);
    }

    /**
     * Get list of all devices configured for Ether Wake.
     *
     * @return array array containing devices configured for Ether Wake
     * @throws Engine_Exception
     */

    public function get_device_list()
    {
        $ethers = new Ethers();
        $devices = array();
        $file = new File(self::FILE_CONFIG);
        if (! $file->exists()) return $devices;
        $lines = $file->get_contents_as_array();
        foreach ($lines as $line) {
            if (! preg_match(
                '/^ether_wake_device\["([a-f0-9:]+)"\]\s*=\s*"(.*)"/i',
                $line, $matches)) continue;
            $parts = explode('|', $matches[2]);
            if (count($parts) == 1) continue;
            $password = '';
            if (count($parts) < 3) list($ifn, $broadcast) = $parts;
            else list($ifn, $broadcast, $password) = $parts;
            $ident = '';
            try {
                $ident = $ethers->get_hostname_by_mac($matches[1]);
            } catch (Engine_Exception $e) {
            }
            $devices[$matches[1]] = array(
                'interface' => $ifn,
                'hostname' => $ident,
                'broadcast' => ($broadcast == 1) ? TRUE : FALSE,
                'password' => $password
            );
        }

        ksort($devices);

        return $devices;
    }

    /**
     * Get device's Ether Wake configuration.
     *
     * @param string $ident Identifier or MAC address of device
     *
     * @return array array containing device's Ether Wake configuration
     * @throws Invalid_Identifier_Exception, Device_Not_Found_Exception
     */

    public function get_device($ident)
    {
        $mac = $this->resolve_ident($ident);
        $devices = $this->get_device_list();
        if (array_key_exists($mac, $devices) === FALSE)
            throw new Device_Not_Found_Exception($ident, CLEAROS_ERROR);
        return $devices[$mac];
    }

    /**
     * Send Wake-on-LAN "magic packet" to device.
     *
     * @param string $ident Identifier or MAC address of device to wake up
     *
     * @return void
     * @throws Invalid_Identifier_Exception, Device_Not_Found_Exception, Send_Wake_Exception
     */

    public function wake_device($ident)
    {
        $device = $this->get_device($ident);
        $args = sprintf('-i %s', escapeshellarg($device['interface']));
        if ($device['broadcast'] === TRUE) $args .= ' -b';
        if (strlen($device['password'])) {
            $args .= sprintf(' -p %s',
                escapeshellarg($device['password']));
        }
        $args .= ' ' . $this->resolve_ident($ident);

        $rc = FALSE;
        try {
            $options = array(
                'validate_exit_code' => FALSE,
                'validate_output' => FALSE
            );
            $shell = new Shell();
            $rc = $shell->execute(
                self::BIN_ETHER_WAKE, $args, TRUE, $options);
        } catch (Validation_Exception $e) {
        } catch (Engine_Exception $e) {
        }
        if ($rc === FALSE)
            throw new Send_Wake_Exception(0, CLEAROS_ERROR);
        else if ($rc > 0)
            throw new Send_Wake_Exception($rc, CLEAROS_ERROR);
    }

    /**
     * Set automated schedule for device.
     *
     * @param string $ident Identifier or MAC address of device to schedule
     * @param string $minute minute of the day
     * @param string $hour hour of the day
     * @param string $day_of_month day of the month
     * @param string $month month
     * @param string $day_of_week day of week
     *
     * @return void
     * @throws Invalid_Identifier_Exception, Device_Not_Found_Exception, Validation_Exception
     */

    public function set_schedule($ident, $minute, $hour, $day_of_month, $month, $day_of_week)
    {
        $mac = $this->resolve_ident($ident);
        $name = sprintf(
            'app-ether-wake-%s', preg_replace('/:/', '-', $mac));
        $command = sprintf(
            '%s/deploy/ether-wake.php --wake -m %s >/dev/null 2>&1',
            realpath(__DIR__ . '/..'), $mac);

        $cron = new Cron();
        $cron->delete_configlet($name);
        $cron->add_configlet_by_parts($name,
            $minute, $hour, $day_of_month, $month, $day_of_week,
            'webconfig', $command);
    }

    /**
     * Get automated schedule for device.
     *
     * @param string $ident Identifier or MAC address of device
     *
     * @return array array containing device's cron schedule.
     * @throws Invalid_Identifier_Exception, Device_Not_Found_Exception
     */

    public function get_schedule($ident)
    {
        $mac = $this->resolve_ident($ident);
        $name = sprintf(
            'app-ether-wake-%s', preg_replace('/:/', '-', $mac));

        $schedule = array();

        try {
            $cron = new Cron();
            $contents = $cron->get_configlet($name);
            $schedule = explode(' ', $contents, 5);
        } catch (Cron_Configlet_Not_Found_Exception $e) {
        }

        return $schedule;
    }

    ///////////////////////////////////////////////////////////////////////////////
    // V A L I D A T I O N   R O U T I N E S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Validates a device identifier (MAC address or hostname).
     *
     * @param string $ident device identifier
     *
     * @return boolean TRUE if valid
     */

    public static function is_valid_ident($ident)
    {
        if (Network_Utils::is_valid_mac($ident))
            return TRUE;

        try {
            $ethers = new Ethers();
            $ethers->get_mac_by_hostname($ident);
        } catch (Engine_Exception $e) {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * Validates a device Wake-on-LAN password.
     *
     * @param string $password device password
     *
     * @return boolean TRUE if valid
     */

    public static function is_valid_password($password)
    {
        if (Network_Utils::is_valid_mac($password)) return TRUE;
        if (preg_match(
            '/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/',
            $password)) return TRUE;

        return FALSE;
    }

    /**
     * Validates a device identifier (MAC address or hostname).
     *
     * @param string $ident device identifier
     *
     * @return string which is empty if valid.
     */

    public static function validate_ident($ident)
    {
        if (Ether_Wake::is_valid_ident($ident)) return '';
        return lang('ether_wake_invalid_identifier');
    }

    /**
     * Validates a device Wake-on-LAN password.
     *
     * @param string $password device password
     *
     * @return string which is empty if valid.
     */

    public static function validate_password($password)
    {
        if (Ether_Wake::is_valid_password($password)) return '';
        return lang('ether_wake_invalid_password');
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
